#ifndef HASHTAB_H
#define HASHTAB_H
typedef void(*htable_clean_callback)(void*);

struct node_struct {
	char* key;
	void* value;
	struct node_struct * next;
};

typedef struct node_struct Node;
struct hashtable_struct {
	int size;
	htable_clean_callback fclean;
	Node **vect;
};

typedef struct hashtable_struct HTable;
//typedef Node ** HTable;
struct iterator_struct {
	int idx;
	Node * cur;
};
typedef struct iterator_struct Iterator;

void htable_create(int size, HTable *ptab, htable_clean_callback clean);

void htable_set(HTable *T,char *key,void* value);

int htable_get(HTable *T,char *key,void **value) ;

void htable_remove(HTable *T,char *key);

int htable_exists(HTable *T,char*key);

void htable_clear(HTable *T);

void htable_iterator_first(HTable *T,Iterator *it);

void htable_iterator_next(HTable *T,Iterator *it);

int htable_iterator_eof(Iterator *it);

char * htable_iterator_pair(Iterator *it,void** value);

#endif
